package example.org;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {

  @Test
  void getAsString() {
    PlayingCard card = new PlayingCard('H', 3, "image_link");
    assertEquals("H3", card.getAsString());
  }

  @Test
  void getSuit() {
    PlayingCard card = new PlayingCard('C', 12, "image_link");
    assertEquals('C', card.getSuit());
  }

  @Test
  void getFace() {
    PlayingCard card = new PlayingCard('S', 1, "image_link");
    assertEquals(1, card.getFace());
  }

  @Test
  void getImageLink() {
    PlayingCard card = new PlayingCard('D', 13, "image_link");
    assertEquals("image_link", card.getImageLink());
  }
}
