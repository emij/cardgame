package example.org;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
  private DeckOfCards deckOfCards;

  @BeforeEach
  public void setUp() {
    deckOfCards = new DeckOfCards();
  }

  @Test
  void getDeck() {
    assertEquals(52, deckOfCards.getDeck().size());
  }

  @Test
  public void testDeckSize() {
    assertEquals(52, deckOfCards.getDeck().size());
  }

  @Test
  public void testDeckContainsCards() {
    ArrayList<String> deck = deckOfCards.getDeck();
    for (char s : deckOfCards.getSuit()) {
      for (char r : deckOfCards.getRank()) {
        String card = r + "" + s;
        assertTrue(deck.contains(card));
      }
    }
  }
}
