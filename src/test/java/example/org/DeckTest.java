package example.org;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DeckTest {

  @Test
  void deal() {
    Deck deck = new Deck();
    ArrayList<PlayingCard> hand = deck.deal(5);
    assertNotNull(hand);
    assertEquals(5, hand.size());
  }

  @Test
  void showHand() {
    // As this method prints output to the console, it cannot be tested with assertions.
    // You can manually check the output when running the application.
    Deck.showHand();
  }

  @Test
  void sumOfHand() {
    ArrayList<PlayingCard> hand = new ArrayList<>();
    hand.add(new PlayingCard('H', 1, ""));
    hand.add(new PlayingCard('D', 2, ""));
    hand.add(new PlayingCard('S', 10, ""));
    int sum = Deck.sumOfHand(hand);
    assertEquals(13, sum);
  }

  @Test
  void getHeartCards() {
    ArrayList<PlayingCard> hand = new ArrayList<>();
    hand.add(new PlayingCard('H', 1, ""));
    hand.add(new PlayingCard('D', 2, ""));
    hand.add(new PlayingCard('S', 10, ""));
    hand.add(new PlayingCard('H', 11, ""));
    hand.add(new PlayingCard('H', 13, ""));
    String heartCards = Deck.getHeartCards(hand);
    assertEquals("H1 H11 H13", heartCards);
  }

  @Test
  void hasFlush() {
    ArrayList<PlayingCard> hand = new ArrayList<>();
    hand.add(new PlayingCard('H', 1, ""));
    hand.add(new PlayingCard('H', 2, ""));
    hand.add(new PlayingCard('H', 10, ""));
    hand.add(new PlayingCard('H', 11, ""));
    hand.add(new PlayingCard('H', 13, ""));
    boolean hasFlush = Deck.hasFlush(hand);
    assertTrue(hasFlush);
  }

  @Test
  void queenOfSpadesExists() {
    ArrayList<PlayingCard> hand = new ArrayList<>();
    hand.add(new PlayingCard('S', 1, ""));
    hand.add(new PlayingCard('S', 2, ""));
    hand.add(new PlayingCard('S', 10, ""));
    hand.add(new PlayingCard('S', 11, ""));
    hand.add(new PlayingCard('S', 12, ""));
    boolean queenOfSpadesExists = Deck.QueenOfSpadesExists(hand);
    assertTrue(queenOfSpadesExists);
  }
}
