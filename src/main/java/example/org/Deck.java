package example.org;

import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Deck {
    private static ArrayList<PlayingCard> cards;

    public Deck() {
        DeckOfCards deckOfCards = new DeckOfCards();
        ArrayList<String> deckStrings = deckOfCards.getDeck();
        cards = new ArrayList<>(deckStrings.size());

        for (String cardString : deckStrings) {
            char suit = cardString.charAt(1);
            int face = rankToValue(cardString.charAt(0));
            boolean pictureCard = false;
            String suitString = "";
            String faceToString = "";
            if (suit == 'S') {
                suitString = "spade";
            } else if (suit == 'H') {
                suitString = "heart";
            } else if (suit == 'D') {
                suitString = "diamond";
            } else if (suit == 'C') {
                suitString = "club";
            }
            if (face == 11) {
                faceToString = "J";
                pictureCard = true;
            } else if (face == 12) {
                faceToString = "Q";
                pictureCard = true;
            } else if (face == 13) {
                faceToString = "K";
                pictureCard = true;
            } else if (face == 1) {
                faceToString = "A";
                pictureCard = true;
            }

            if (pictureCard) {
                cards.add(new PlayingCard(suit, face,
                        "" + suitString + "/800px-Playing_card_" + suitString + "_" + faceToString +
                            ".svg.png"));
            } else {
                cards.add(new PlayingCard(suit, face,
                        "" + suitString + "/800px-Playing_card_" + suitString + "_" + face + ".svg.png"));
            }
        }
    }

    public static ArrayList<PlayingCard> deal(int numCards) {
        Collections.shuffle(cards);
        ArrayList<PlayingCard> hand = new ArrayList<>(numCards);
        for (int i = 0; i < numCards; i++) {
            hand.add(cards.get(i));
        }

        return hand;
    }

    private int rankToValue(char rankChar) {
        switch (rankChar) {
            case 'A':
                return 1;
            case 'T':
                return 10;
            case 'J':
                return 11;
            case 'Q':
                return 12;
            case 'K':
                return 13;
            default:
                return Character.getNumericValue(rankChar);
        }
    }

    public static void showHand() {
        Deck newDeck = new Deck();
        ArrayList<PlayingCard> cards = newDeck.deal(5);
        for (PlayingCard card : cards) {
            {
                System.out.println(card.toString());
            }

        }
    }
    public static int sumOfHand(ArrayList<PlayingCard> hand) {
        if (hand == null){
            return 0;
        }
        return hand.stream()
            .mapToInt(PlayingCard::getFace)
            .sum();
    }

    public static String getHeartCards(ArrayList<PlayingCard> hand) {
        if (hand == null){
            return "";
        }else{
        String heartCards = hand.stream()
            .filter(card -> card.getSuit() == 'H')
            .map(card -> card.getSuit() + String.valueOf(card.getFace()))
            .map(Object::toString) // Convert to String
            .collect(Collectors.joining(" "));
        return heartCards;}
    }

    public static boolean hasFlush(ArrayList<PlayingCard> hand) {
        if (hand == null){
            return false;
        }
        return hand.stream()
            .map(PlayingCard::getSuit)
            .distinct()
            .count() == 1;
    }

    public static boolean QueenOfSpadesExists(ArrayList<PlayingCard> hand) {
        if (hand == null){
            return false;
        }
        return hand.stream()
            .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    }
}