package example.org;

import javafx.application.Application;

public class App {
    public static void main(String[] args) {
        Application.launch(CardGameWindow.class);
    }
}