package example.org;

import com.sun.javafx.charts.Legend;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

public class CardGameWindow extends Application {
    private static Text resultText;
    private static ArrayList<PlayingCard> hand;

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Create the root node, a grid pane with 1 row and 5 columns
        GridPane root = new GridPane();
        root.setHgap(10);
        root.setVgap(20);

        // Add five empty card placeholders to the grid pane
        for (int i = 0; i < 5; i++) {
            StackPane cardPlaceholder = createEmptyCardPlaceholder(200, 300);
            root.add(cardPlaceholder, i, 0);
        }

        // Create the "Draw" and "Check" buttons
        Button drawButton = new Button("Draw");
        Button checkButton = new Button("Check");
        drawButton.setPrefWidth(100);
        checkButton.setPrefWidth(100);



        Text resultText = new Text("Sum of cards: "); // create a new Text node for the
        Text heartOfCards = new Text("Heart of cards: ");
        Text queenOfSpadesExists = new Text("Queen of Spades Exists? ");
        Text isFlush = new Text("Is the hand a 5-flush? ");

        checkButton.setOnAction(event -> {
            int sum = Deck.sumOfHand(hand);
            resultText.setText("Sum of cards: " + sum);
            heartOfCards.setText("Heart of cards: " + Deck.getHeartCards(hand));
            if (Deck.QueenOfSpadesExists(hand)) {
                queenOfSpadesExists.setText("Queen of Spades Exists? Yes");
            } else {
                queenOfSpadesExists.setText("Queen of Spades Exists? No");
            }
            if (Deck.hasFlush(hand)) {
                isFlush.setText("Is the hand a 5-flush? Yes");
            } else {
                isFlush.setText("Is the hand a 5-flush? No");
            }
        });

        drawButton.setOnAction(event -> {
            Deck deck = new Deck();
            hand = Deck.deal(5);
            for (int i = 0; i < 5; i++) {
                StackPane cardPlaceholder = createCard(hand.get(i).getImageLink());
                root.add(cardPlaceholder, i, 0);
            }
        });

        // Create a horizontal box to hold the buttons
        HBox buttonBox = new HBox(20, drawButton, checkButton);
        buttonBox.setAlignment(Pos.CENTER);

        // Create a stack pane for the result text bubble
        VBox resultBox = new VBox(10); // 10 is the gap between panes

        StackPane resultPane = createSumPane(resultText);
        StackPane heartOfCardsPane = createHeartOfCardsPane(heartOfCards);
        StackPane QueenOfSpadesPane = createQueenOfSpadesExistsPane(queenOfSpadesExists);
        StackPane isFlushPane = createIsFlushPane(isFlush);



        resultBox.getChildren().add(resultPane);
        resultBox.getChildren().add(heartOfCardsPane);
        resultBox.getChildren().add(QueenOfSpadesPane);
        resultBox.getChildren().add(isFlushPane);

        // Add the button box and result pane to the grid pane
        root.add(buttonBox, 0, 1, 5, 1);
        root.add(resultBox, 5, 0, 2, 5);
        // add the result pane to the right side of the grid
        // pane

        // Set the padding and alignment of the root node
        root.setPadding(new Insets(20));
        root.setAlignment(Pos.CENTER);

        // Create the scene and show the window
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Card Game");
        primaryStage.show();
    }

    private StackPane createSumPane(Text resultText) {
        StackPane resultPane = new StackPane();
        resultPane.setPrefSize(50, 50);
        resultPane.setPadding(new Insets(10, 20, 10, 10));
        resultPane.setMaxHeight(50);
        resultPane.setStyle("-fx-background-color: #F0E68C; -fx-background-radius: 20;");

        resultPane.getChildren().add(resultText);

        return resultPane;
    }

    private StackPane createHeartOfCardsPane(Text heartOfCards){
        StackPane heartOfCardsPane = new StackPane();
        heartOfCardsPane.setPrefSize(50, 50);
        heartOfCardsPane.setPadding(new Insets(10, 20, 10, 10));
        heartOfCardsPane.setMaxHeight(50);
        heartOfCardsPane.setStyle("-fx-background-color: #F0E68C; -fx-background-radius: 20;");

        heartOfCardsPane.getChildren().add(heartOfCards);

        return heartOfCardsPane;
    }

    private StackPane createQueenOfSpadesExistsPane(Text QueenOfSpadesExists){
        StackPane QueenOfSpadesExistsPane = new StackPane();
        QueenOfSpadesExistsPane.setPrefSize(50, 50);
        QueenOfSpadesExistsPane.setPadding(new Insets(10, 20, 10, 10));
        QueenOfSpadesExistsPane.setMaxHeight(50);
        QueenOfSpadesExistsPane.setStyle("-fx-background-color: #F0E68C; -fx-background-radius: 20;");

        QueenOfSpadesExistsPane.getChildren().add(QueenOfSpadesExists);

        return QueenOfSpadesExistsPane;
    }

    private StackPane createIsFlushPane(Text IsFlush){
        StackPane IsFlushPane = new StackPane();
        IsFlushPane.setPrefSize(50, 50);
        IsFlushPane.setPadding(new Insets(10, 20, 10, 10));
        IsFlushPane.setMaxHeight(50);
        IsFlushPane.setStyle("-fx-background-color: #F0E68C; -fx-background-radius: 20;");

        IsFlushPane.getChildren().add(IsFlush);

        return IsFlushPane;
    }

    private StackPane createCard(String imageLink) {
        // Create a stack pane to hold the card image, text, and other nodes
        StackPane cardPlaceholder = new StackPane();
        cardPlaceholder.setPrefSize(200, 300);
        cardPlaceholder.setStyle("-fx-background-color: white;");

        // Create an image view for the center image
        ImageView centerImageView = new ImageView(new Image(imageLink));
        centerImageView.setPreserveRatio(true);
        centerImageView.setFitWidth(cardPlaceholder.getPrefWidth());
        centerImageView.setFitHeight(cardPlaceholder.getPrefHeight());

        // Set the padding of the stack pane to zero
        cardPlaceholder.setPadding(Insets.EMPTY);

        cardPlaceholder.getChildren().addAll(centerImageView);
        StackPane.setAlignment(centerImageView, Pos.CENTER);

        return cardPlaceholder;
    }

    private StackPane createEmptyCardPlaceholder(double width, double height) {
        // Create a stack pane to hold the card image, text, and other nodes
        StackPane cardPlaceholder = new StackPane();
        cardPlaceholder.setPrefSize(200, 300);
        cardPlaceholder.setStyle("-fx-background-color: white;");

        // Create an image view for the center image;

        // Set the padding of the stack pane to zero
        cardPlaceholder.setPadding(Insets.EMPTY);

        return cardPlaceholder;
    }




    public static void main(String[] args) {
        launch(args);}
}
