package example.org;

import java.util.ArrayList;

public class DeckOfCards {
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private final char[] rank = {'A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K'};
    private ArrayList<String> deck;

    public DeckOfCards() {
        deck = new ArrayList<>();
        for (char s : suit) {
            for (char r : rank) {
                deck.add(r + "" + s);
            }
        }
    }

    public ArrayList<String> getDeck() {
        return deck;
    }

    public char[] getSuit() {
        return suit;
    }

    public char[] getRank() {
        return rank;
    }
}